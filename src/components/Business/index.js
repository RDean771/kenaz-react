import React, { Component } from 'react';
import './style.css';
import BusinessContent from '../BusinessContent';
class Business extends Component {

  render() {
    return (
      <div className="content-business">
        <div className="content-business-header"> 
          <div className="content-business-header-title"> Business </div>
          <div className="content-business-header-link"><a href="#0"> See all </a></div>
        </div>
        {this.props.articles
            .map((article, key) => (
              <BusinessContent key={key} article={article}/>
            ))
          }
      </div>
    );
  }
}

export default Business;
