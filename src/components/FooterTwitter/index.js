import React, { Component } from 'react';
import './style.css';
import FooterTwitterPost from '../FooterTwitterPost';

class FooterTwitter extends Component {

  render() {
    return (
        <div className="footer-twitter-item">
          <div className="footer-twitter-item-row"> Twitter Feed </div>
          <FooterTwitterPost twitterpost="Is this your typical million dollar day in the park? 
            http://enva.to/150vxFC  Happy @TrueThemes Day! #ThemeForest pic.twitter.com/EHz7awxOXy"/>
          <FooterTwitterPost twitterpost="Happy TrueThemes Day http://enva.to/1dRzgLD "/>
          <FooterTwitterPost twitterpost="@robscri I would really want to look into what's taking so long. Thank you ever so much! ^TC"/>
        </div>
    );
  }
}

export default FooterTwitter;
