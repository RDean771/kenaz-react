import React, { Component } from 'react';
import './style.css';

class FooterTwitterPost extends Component {

  render() {
    return (
          <div className="footer-twitter-post-item-row">
            <div className="footer-twitter-post-item-row-envato">Envato @envato </div>
            <div className="footer-twitter-post-item-row-post"> {this.props.twitterpost}</div>
          </div>
    );
  }
}

export default FooterTwitterPost;
