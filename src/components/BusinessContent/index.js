import React, { Component } from 'react';
import './style.css';
import slika from '../../assets/img/Layer-56.png';
class BusinessContent extends Component {
  months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
  convertDate = (date) => {
    const temp=date.split('T');
    const datum=temp[0].split('-').map(x => parseInt(x, 10));
    datum.reverse();
    
    return this.months[datum[1] - 1 ] + ' ' + datum[0].toString() + ', ' + datum[2].toString();

  }

  render() {
    return (
        <div className="content-business-content">
        <a href={this.props.article.url}> <div className="content-business-content-image" style={
              {backgroundImage: `url(${!this.props.article.urlToImage? slika : this.props.article.urlToImage})`, 
               height: '100px',
               backgroundSize: 'cover'
               }}></div>
            </a>
        <div className="content-business-content-text">
          <span className="content-business-content-text-date"> {this.convertDate(this.props.article.publishedAt)} </span>
          <div className="content-business-content-text-paragraph"> <a href={this.props.article.url}>{this.props.article.title} </a> </div>

        </div>
      
      </div>
    );
  }
}

export default BusinessContent;
