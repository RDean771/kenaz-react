import React, { Component } from 'react';
import './style.css';
import FooterImagePost from '../FooterImagePost';

class FooterRecentNews extends Component {

  render() {
    return (
        <div className="footer-recentnews-item">
        <div className="footer-recentnews-item-header"> {this.props.headertitle}  </div>
        <FooterImagePost border="enable" fontColor="#ccc" dateColor="#666"/>
        <FooterImagePost border="enable" fontColor="#ccc" dateColor="#666"/>
        <FooterImagePost border="disable" fontColor="#ccc" dateColor="#666"/>
      </div>
    );
  }
}

export default FooterRecentNews;
