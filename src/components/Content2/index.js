import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './style.css';

class Content2 extends Component {

  render() {
    return (
      <div className="content2">
        <Link to="/" className="content2-link"> <h1> Go to Content 1 </h1> </Link>
      </div>
    );
  }
}

export default Content2;
