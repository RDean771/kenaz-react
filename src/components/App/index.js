import React, { Component } from 'react';
import './App.css';

import { Route } from 'react-router-dom';
import Header from '../Header';
import Content from '../Content';
import Footer from '../Footer';
import Content2 from '../Content2';
class App extends Component {

  render() {
    return (
      <div className="App">
        <Header />
        <Route exact path="/" component = {Content} />
        <Route exact path="/content2" component = {Content2} />
        <Footer />
      </div>
    );
  }
}

export default App;
