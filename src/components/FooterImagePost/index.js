import React, { Component } from 'react';
import './style.css';
class FooterImagePost extends Component {

  render() {
    return (
        <div>
        <div className="footer-imagepost">
          <div className="footer-imagepost-text">
            <div className="footer-imagepost-text-about" style={{color: this.props.dateColor}}>
              <div className="footer-imagepost-text-about-date">
               August 26,2013
              </div>
              <div className="footer-imagepost-text-about-commentdiv"><div className="footer-imagepost-text-about-comment" />5</div>
            </div>
            <div className="footer-imagepost-text-post" style={{color: this.props.fontColor}}>
            Palestinians call off peace talks after clash

            </div>
          
          </div>
          <div className="footer-imagepost-image">
          </div>
        </div>
    {(this.props.border === "enable" && <div className="footer-imagepost-hr"/>) || <div className="footer-imagepost-hr2"/>}
        </div>
    );
  }
}

export default FooterImagePost;
