import React, { Component } from 'react';
import './style.css';

class FooterKenaz extends Component {

  render() {
    return (
        <div className="footer-kenaz-item">
          <div className="footer-kenaz-item-row">
            <div className="footer-kenaz-logo"></div>
            <div className="footer-kenaz-text">Kenaz</div>
          </div>
          <div className="footer-kenaz-item-row">
            <div className="footer-kenaz-lorem">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus leo ante.</div>
          </div>
          <div className="footer-kenaz-item-row-justify-content-between">
            <div className="footer-kenaz-image wifi-image"></div>
            <div className="footer-kenaz-image facebook-image"></div>
            <div className="footer-kenaz-image twitter-image"></div>
            <div className="footer-kenaz-image dribbble-image"></div>
            <div className="footer-kenaz-image linked-in-image"></div>
            <div className="footer-kenaz-image youtube-image"></div>
            <div className="footer-kenaz-image skype-image"></div>
            
          </div>
        </div>
    );
  }
}

export default FooterKenaz;
