import React, { Component } from 'react';
import './style.css';
import FooterLine from '../FooterLine';
import FooterContent from '../FooterContent';
import FooterSeparator from '../FooterSeparator';
import FooterNavigation from '../FooterNavigation';
import FooterEnd from '../FooterEnd';
class Footer extends Component {

  render() {
    return (
      <div className="footer">
        <FooterLine />
        <FooterContent />
        <FooterSeparator />
        <FooterNavigation />
        <FooterEnd />
      </div>
    );
  }
}

export default Footer;
