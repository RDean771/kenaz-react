import React, { Component } from 'react';
import './style.css';
class FooterNavigation extends Component {

  render() {
    return (
      <div className="footer-navigation-wrapper">
        <div className="footer-navigation-wrapper-content">
          <div className="footer-navigation-wrapper-content-cc"> © 2013 - Kenaz Template - Proudly made at Plava tvornica Croatia </div>
          <div className="footer-navigation-wrapper-content-navbar"> Typography  -  Templates  -  Contact Form  -  404 Page </div>
        </div>
      </div>
    );
  }
}

export default FooterNavigation;
