import React, { Component } from 'react';
import './style.css';

class FooterNewsletter extends Component {

  render() {
    return (
        <div className="footer-newsletter-item">
          <div className="footer-newsletter-item-row">
            <div className="footer-newsletter-title">Newsletter</div>
          </div>
          <div className="footer-newsletter-item-row">
            <div className="footer-newsletter-lorem">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus leo ante.</div>
          </div>
          <div className="footer-newsletter-item-row">
                <input type="text" className="footer-newsletter-input" placeholder="Your email"/>
                <div className="footer-newsletter-button">Subscribe</div>
          </div>
        </div>
    );
  }
}

export default FooterNewsletter;
