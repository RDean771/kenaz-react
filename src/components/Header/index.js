import React, { Component } from 'react';
import HeaderKenaz from '../HeaderKenaz';
import HeaderNavigation from '../HeaderNavigation';
import './style.css';

class Header extends Component {

  render() {
    return (
      <div className="header">
        <HeaderKenaz />
        <HeaderNavigation />
      </div>
    );
  }
}

export default Header;
