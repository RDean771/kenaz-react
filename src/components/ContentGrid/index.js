import React, { Component } from 'react';
import './style.css';
import ContentNewsSport from '../ContentNewsSport';
import Placeholder620 from '../Placeholder620';
import Business from '../Business';
import NewsCarousel from '../NewsCarousel';
import Editorials from '../Editorials';
import FooterImagePost from '../FooterImagePost';
import axios from 'axios';
import YouTube from 'react-youtube'

class ContentGrid extends Component {
  state = {
    newsArticles : [],
    sportArticles: [],
    businessArticles: [],
    opts : {
      height: '200',
      width: '300',
      playerVars: { // https://developers.google.com/youtube/player_parameters
        autoplay: 0
      }
    },
    newsCarouselArticles: []
  }
  index = 1
  slideRight = () => {
    this.index+=1;
    if (this.index === 10) this.index = 1
    axios.get(`https://newsapi.org/v2/top-headlines?pageSize=2&page=${this.index}&country=us&category=technology&apiKey=b3a5e4f08c7d482d83dd36821538c571`)
      .then(res => {
        this.setState({newsCarouselArticles: res.data.articles})
      })

  }
  slideLeft = () => {
    this.index-= 1;
    if (this.index === 0) this.index = 9;
    axios.get(`https://newsapi.org/v2/top-headlines?pageSize=2&page=${this.index}&country=us&category=technology&apiKey=b3a5e4f08c7d482d83dd36821538c571`)
      .then(res => {
        this.setState({newsCarouselArticles: res.data.articles})
      })

  }

  componentDidMount(){
    let myNewsArticles = [], mySportArticles=[], myBusinessArticles=[], myNewsCarouselArticles=[];
    axios.get('https://newsapi.org/v2/top-headlines?country=us&apiKey=b3a5e4f08c7d482d83dd36821538c571')
      .then(res => {
        myNewsArticles.push(res.data.articles);
        this.setState({newsArticles: myNewsArticles[0].slice(0,3)});
      })
    axios.get('https://newsapi.org/v2/top-headlines?category=sports&country=us&apiKey=b3a5e4f08c7d482d83dd36821538c571')
      .then(res => {
        mySportArticles.push(res.data.articles);
        this.setState({sportArticles: mySportArticles[0].slice(0,3)});
      })
      axios.get('https://newsapi.org/v2/top-headlines?category=business&country=us&apiKey=b3a5e4f08c7d482d83dd36821538c571')
      .then(res => {
        myBusinessArticles.push(res.data.articles);
        this.setState({businessArticles: myBusinessArticles[0].slice(0,4)});
      })
      axios.get('https://newsapi.org/v2/top-headlines?pageSize=2&page=1&country=us&category=technology&apiKey=b3a5e4f08c7d482d83dd36821538c571')
      .then(res => {
        myNewsCarouselArticles.push(res.data.articles);
        this.setState({newsCarouselArticles: myNewsCarouselArticles[0]});
      })
  }

  render() {
    return (
      <div className="content-grid">
      <div className="content-grid-row1">
        <ContentNewsSport title="Top News" bcolor="#3677B5" comms={['14','22','5']} articles={this.state.newsArticles}/>
      </div>
      <div className="content-grid-right-col">
        <div className="content-grid-right-col-nav">
            <a href="/" className="content-grid-right-col-nav-item-active"> Popular </a>
            <a href="/" className="content-grid-right-col-nav-item"> Top Rated </a>
            <a href="/" className="content-grid-right-col-nav-item"> Comments </a>
        </div>
        <div className="content-grid-right-col-image-post">
          <FooterImagePost border="enable" fontColor="#fff" dateColor="#ACB3BA" />
        </div>
        <div className="content-grid-right-col-image-post">
          <FooterImagePost border="enable" fontColor="#fff" dateColor="#ACB3BA"/>
        </div>
        <div className="content-grid-right-col-image-post">
          <FooterImagePost border="enable" fontColor="#fff" dateColor="#ACB3BA"/>
        </div>
        <div className="content-grid-right-col-image-post">
          <FooterImagePost border="enable" fontColor="#fff" dateColor="#ACB3BA"/>
        </div>
        <div className="content-grid-right-col-image-post">
          <FooterImagePost border="enable" fontColor="#fff" dateColor="#ACB3BA"/>
        </div>
      
      </div>
      <div className="content-grid-right-col-video-div">
          <div className="content-grid-right-col-video-div-title">Kenaz TV </div>
          <YouTube videoId="A71aqufiNtQ" opts={this.state.opts} onReady={this._onReady} />
          <div className="content-grid-right-col-video-div-description">
          <span className="content-business-content-text-date pl20"> August 23, 2013</span>
          <div className="content-business-content-text-paragraph pl20"> Crash course about React </div>
          </div>
      </div>
      <div className="content-grid-right-col-placeholder-div">
        <div className="content-grid-right-col-placeholder-div-banner">
        banner
        <br/>
        300x820
        </div>

      </div>
      <div className="content-grid-row2">
        <ContentNewsSport title="Sport" bcolor="#84C14F" comms={['0','0','0']} articles={this.state.sportArticles} />
      </div>
      <div className="content-grid-row3">
        <Placeholder620 />
      </div>
      <div className="content-grid-row4">
        <Business articles={this.state.businessArticles}/>
      </div>
      <div className="content-grid-row5">
        <Placeholder620 />
      </div>
      <div className="content-grid-row6">
        <NewsCarousel articles={this.state.newsCarouselArticles} slideLeft={this.slideLeft} slideRight={this.slideRight}/>
      </div>
      <div className="content-grid-row7">
        <Editorials articles={this.state.newsCarouselArticles}/>
      </div>
      </div>
    );
  }
}

export default ContentGrid;
