import React, { Component } from 'react';
import './style.css';
import NewsSportImagePost from '../NewsSportImagePost';
class ContentNewsSport extends Component {
  render() {
    return (
      <div className="content-news-sport" style={{borderLeft: `10px solid ${this.props.bcolor}`}}>
        <div className="content-news-sport-header"> 
          <div className="content-news-sport-header-title"> {this.props.title} </div>
          <div className="content-news-sport-header-link"><a href="#0"> See all </a></div>
        </div>
        
          {this.props.articles
            .map((article, key) => (
              <div key={key} className="content-news-sport-content">
              <NewsSportImagePost key={key} article={article}/>
              </div>
            ))
          }
        
      </div>
    );
  }
}

export default ContentNewsSport;
