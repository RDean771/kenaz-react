import React, { Component } from 'react';
import './style.css';

class HeaderKenaz extends Component {
  state = {
    pressed : false,
    animation : false
  }
  toggle = () => {
    const prevState = this.state.pressed;
    const prevAnimation = this.state.animation;
    this.setState({animation: !prevAnimation});
    if (this.state.pressed) {
      setTimeout(() => {
        this.setState({pressed: !prevState});
      }, 1000);
    } else {
      this.setState({pressed: !prevState});
    }

  }
  render() {
    return (
      <div className="header-kenaz">
        <div className="header-kenaz-wrapper">
          <div className="kenaz-logo"></div>
          <div className="kenaz-text"> Kenaz</div>
          <div className="kenaz-navigation-items">
            <div className="kenaz-navigation-item"> Media </div>
            <div className="kenaz-navigation-item"> Marketing </div>
            <div className="kenaz-navigation-item"> Contact </div>
            {this.state.pressed && <input className={!this.state.animation ? 'input2' : 'input'} type='text' placeholder="Search by" />}
            <div className="kenaz-search" onClick={this.toggle}></div>
          </div>
        </div>
      </div>
      
    );
  }
}

export default HeaderKenaz;
