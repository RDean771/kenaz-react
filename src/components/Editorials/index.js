import React, { Component } from 'react';
import './style.css';
import NewsCarouselContent from '../NewsCarouselContent';
import leftarrow from '../../assets/img/Shape-3.png';
import rightarrow from '../../assets/img/Shape-4.png';
class Editorials extends Component {

  render() {
    return (
      <div className="content-editorials">
        <div className="content-editorial">
          <div className="content-editorial-header"> Editorials
            <div className="content-editorial-header-arrows">
            <img src={leftarrow} alt="" />
            <img src={rightarrow} alt=""/>
             </div>
          
          </div>
          <NewsCarouselContent article={this.props.articles[0]}/>

        </div>
        <div className="content-local-news">
          <div className="content-local-news-header"> Local News 
          <div className="content-local-news-header-arrows">
            <img src={leftarrow} alt="" />
            <img src={rightarrow} alt="" />
             </div>
        </div>
        <NewsCarouselContent article={this.props.articles[1]}/>

        </div>
      </div>
    );
  }
}

export default Editorials;
