import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './style.css';
import Placeholder940 from '../Placeholder940';
import Slider from '../Slider';
import ContentGrid from '../ContentGrid';

class Content extends Component {

  render() {
    return (
      <div className="content">
        <Link to="/content2" className="content-link"> <h1> Go to Content 2 </h1> </Link>
      <div className="content-wrapper">
        <Placeholder940 />
        <Slider/>
        <ContentGrid />
        <Placeholder940 />
      </div>
      </div>
    );
  }
}

export default Content;
