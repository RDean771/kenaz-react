import React, { Component } from 'react';
import './style.css';
import slika from '../../assets/img/Layer-59.png';
class NewsSportImagePost extends Component {
  months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
  convertDate = (date) => {
    const temp=date.split('T');
    const datum=temp[0].split('-').map(x => parseInt(x, 10));
    datum.reverse();
    
    return this.months[datum[1] - 1 ] + ' ' + datum[0].toString() + ', ' + datum[2].toString();

  }
  render() {
    return (
        <div className="content-news-sport-content-image-post">
            <a href={this.props.article && (this.props.article.url  || "#0")}> <div className="content-news-sport-content-image-post-image" style={
              {backgroundImage: `url(${this.props.article && (!this.props.article.urlToImage? slika: this.props.article.urlToImage)})`, 
               height: '100px',
               backgroundSize: 'cover',
               boxSizing: 'border-box'
               }}></div>
            </a>
            <div className="content-news-sport-content-image-post-text">
              <span className="content-news-sport-content-image-post-text-date"> {this.convertDate(this.props.article.publishedAt)} </span> <br/>
              <div className="content-news-sport-content-image-post-paragraph"> <a href={this.props.article.url}>{this.props.article.title} </a></div>
            </div>

        </div>
           );
        }
      }
      
export default NewsSportImagePost;