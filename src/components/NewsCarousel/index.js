import React, { Component } from 'react';
import './style.css';
import leftarrow from '../../assets/img/Shape-1.png';
import rightarrow from '../../assets/img/Shape-2.png';
import NewsCarouselContent from '../NewsCarouselContent';
class NewsCarousel extends Component {
  state = {
    articles: []
  }
  

  render() {
    return (
      <div className="content-news-carousel">
        <div className="content-news-carousel-header"> News Carousel
        <div className="content-news-carousel-header-arrows">
          <img src={leftarrow} alt="" onClick={() => this.props.slideLeft()}/>
          <img src={rightarrow} alt="" onClick={() => this.props.slideRight()}/>
        </div>

        </div>
        <div className="content-news-carousel-content">
        {!!this.props.articles.length && this.props.articles.map((article,index) => (
        <NewsCarouselContent key={index} article={article}/>))}

        </div>
     
        </div>
    );
  }
}

export default NewsCarousel;
