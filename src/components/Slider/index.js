import React, { Component } from 'react';
import './style.css';
import photo1 from '../../assets/img/Layer-48.png';
import photo2 from '../../assets/img/Layer-35.png';
import photo3 from '../../assets/img/Layer-30.png';

class Slider extends Component {
  state = {
    activePicture : `url(${photo1})`,
    imageUrls : [`url(${photo1})`,
                 `url(${photo2})`,
                 `url(${photo3})`,
                ]
  }
  slideLeft = () => {
    let array = this.state.imageUrls.slice();
    const last = array.pop();
    array.splice(0,0,last);
    const newactivePicture = array[0];
    this.setState({
      activePicture : newactivePicture,
      imageUrls : array
    })
  }
  slideRight = () => {
    let array = this.state.imageUrls.slice();
    array.reverse();
    const first = array.pop();
    array.reverse();
    array.push(first);
    const newactivePicture = array[0];
    this.setState({
      activePicture : newactivePicture,
      imageUrls : array
    })
  }
  render() {
    return (
      <div className="slider-div" style={{background : this.state.activePicture}}>
        <div className="slider-div-arrows">
         <div className="slider-div-arrows-left" onClick={() => this.slideLeft()}> </div>
         <div className="slider-div-arrows-right" onClick={() => this.slideRight()}> </div>
        </div>
      </div>
    );
  }
}

export default Slider;
