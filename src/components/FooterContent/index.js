import React, { Component } from 'react';
import './style.css';
import FooterKenaz from '../FooterKenaz';
import FooterNewsletter from '../FooterNewsletter';
import FooterTags from '../FooterTags';
import FooterRecentNews from '../FooterRecentNews';
import FooterTwitter from '../FooterTwitter';
class FooterContent extends Component {

  render() {
    return (
      <div className="footer-content">
        <div className="footer-content-wrapper">
          <div className="footer-grid">
            <div className="footer-row">
              <FooterKenaz />
              <FooterNewsletter />
              <FooterTags />
            </div>
            <div className="footer-row">
              <FooterRecentNews headertitle="Featured"/>
              <FooterRecentNews headertitle="Random Posts"/>
              <FooterTwitter />
            </div>
          </div>

        
        </div>
      </div>
    );
  }
}

export default FooterContent;
