import React, { Component } from 'react';
import './style.css';

class FooterTag extends Component {

  render() {
    return (
            <div className="footer-tags-item-row-tag"> {this.props.tagName}</div>
    );
  }
}

export default FooterTag;
