import React, { Component } from 'react';
import './style.css';
import slika from '../../assets/img/Layer-52.png';
class NewsCarouselContent extends Component {
  months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
  convertDate = (date) => {
    const temp=date.split('T');
    const datum=temp[0].split('-').map(x => parseInt(x, 10));
    datum.reverse();
    
    return this.months[datum[1] - 1 ] + ' ' + datum[0].toString() + ', ' + datum[2].toString();

  }

  render() {
    return (
        <div className="content-news-carousel-content-image-post">
        <a href={this.props.article && (this.props.article.url || "#0")}> 
              <div className="content-news-carousel-image-post-image" style={
              {backgroundImage: `url(${this.props.article && (this.props.article.urlToImage? this.props.article.urlToImage : slika)})`, 
               height: '100px',
               backgroundSize: 'cover'
               }}></div>
            </a>
            <div className="content-news-sport-content-image-post-text">
              <span className="content-news-sport-content-image-post-text-date"> {this.props.article && (this.convertDate(this.props.article.publishedAt) || "1111")} </span> <br/>
              <div className="content-news-sport-content-image-post-paragraph"> <a href={this.props.article && (this.props.article.url || "#0")}>
              {this.props.article && (this.props.article.title || "#0")} </a></div>
            </div>

      </div>
    );
  }
}

export default NewsCarouselContent;
