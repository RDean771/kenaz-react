import React, { Component } from 'react';
import './style.css';
import FooterTag from '../FooterTag';

class FooterTags extends Component {

  render() {
    return (
        <div className="footer-tags-item">
          <div className="footer-tags-item-row">
            <div className="footer-tags-item-row-title"> Tags Widget </div>
          </div>
          <div className="footer-tags-item-row">
            <FooterTag tagName="assueverit"/>
            <FooterTag tagName="utroquce"/>
            <FooterTag tagName="probo"/>
            <FooterTag tagName="assuev"/>
            <FooterTag tagName="probo"/>
            <FooterTag tagName="assuevverit"/>
            <FooterTag tagName="titl"/>
            <FooterTag tagName="assuevverit"/>
            <FooterTag tagName="assuevverit"/>
            <FooterTag tagName="utroquce"/>
            <FooterTag tagName="probo"/>
            <FooterTag tagName="assuevverit"/>
            <FooterTag tagName="utroquce"/>

          </div>
        </div>
    );
  }
}

export default FooterTags;
