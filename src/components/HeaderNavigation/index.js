import React, { Component } from 'react';
import './style.css';

class HeaderNavigation extends Component {
  
  render() {
    return (
      <div className="header-topics">
        <div className="header-topics-wrapper">
          <div className="header-topics-topic border-1"> NEWS </div>
          <div className="header-topics-topic border-2"> BUSINESS </div>
          <div className="header-topics-topic border-3"> SPORT </div>
          <div className="header-topics-topic border-4"> LIFE </div>
          <div className="header-topics-topic border-5"> TECH </div>
          <div className="header-topics-topic border-6"> TRAVEL </div>
          
        </div>
      </div>
      
    );
  }
}

export default HeaderNavigation;
